# Pi calculus interpreter Haskell (π-calculus)

A [Haskell](https://en.wikipedia.org/wiki/Haskell_(programming_language)) 
implementation of the [pi-calculus](https://en.wikipedia.org/wiki/%CE%A0-calculus)


## The π-calculus
π-calculus is a [*process calculi*](https://en.wikipedia.org/wiki/Process_calculus) 
particularly useful to reason about concurrent computations.
It has some applications in **business processes** and **molecular biology**.

## This implementation
This implementation consists of both a parser and an interpreter.  
It uses Haskell threads from `Control.Concurrent` to implement pi-calculus operations (`split` and `repeat` operations).  
The parser uses the Functional parsing library from *chapter 13 of Programming in Haskell, Graham Hutton, Cambridge University Press, 2016*.   
Professor Hutton's Functional Parsing Library: http://bit.ly/C_FunctParsLib   
Tutorial on Functional Parsing: [Youtube, Functional Parsing - Computerphile](https://www.youtube.com/watch?v=dDtZLm7HIJs)  
## USAGE
To execute a program use `exe`

 - example: `exe "!0"`

To only parse the program (without executing it) use `parse programP`

 - example: `parse programP "!0"`

Some more interesting programs:
 - `simple message passing: "(new x)(^x<x>.0|x(x).0)"`
 - `"(new general)((new bridge)^general<bridge>.bridge(sink).!0|general(rcvBridge).^rcvBridge<general>.0)"`

## Running tests
To run the (small) test suite use the funcion `runtests`.<br>
To test the parser only, use the function `parsing'test`.

# TODO
 - [ ] Making the implementation synchronous
 - [ ] Adding a type system:
    * [ ] simply typed π-calculus
    * [ ] generic types
    * [ ] dependent types
    * [ ] linear types
       - linear types may be particularly useful for dealing with resource-like channels
 - [ ] Adding primitive datatypes 
 - [ ] Designing a better syntax

# Useful resources

 - [Wikipedia page](https://en.wikipedia.org/wiki/%CE%A0-calculus)

# TODO
Add to this readme some interesting papers and resources
