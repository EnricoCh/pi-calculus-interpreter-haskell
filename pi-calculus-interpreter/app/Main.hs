module Main where

import Parsing
import Debug.Trace
import Control.Concurrent
import Data.List

main :: IO ()
main = undefined


type Extra'Value = String

data Pi'Value = C (Chan Pi'Value) Extra'Value
instance Show Pi'Value where 
    show (C c extra'value) = extra'value
mkChan extra'value = do
    chan <- newChan
    pure $ C chan extra'value 
mkChan' = mkChan ""
getChan (C chan value) = chan

type ChannelName = String 

type ChannelList = [(ChannelName, Pi'Value)] 

data Program = Receive ChannelName ChannelName Program
             | Send ChannelName ChannelName Program
             | Split Program Program
             | Create ChannelName Program
             | Repeat Program
             | Terminate
  deriving (Show)

exe :: String -> IO ()
exe s = do
        forkIO $ execute [] $ fst $ head $ parse programP s
        threadDelay 1000000 -- to let threads finish: there's no wait in Control.Concurrent

execute :: ChannelList -> Program -> IO ()
execute channelList (Receive channelname1 channelname2 program) = do
    msg <- receiveOnChanList channelList channelname1
    putStrLn $ show msg ++ "-from-" ++ show channelname1 ++ "-as-" ++ show channelname2  
    execute ((channelname2,msg):channelList) program
execute channelList (Send channelname1 channelname2 program) = do
    putStrLn $ show channelname2 ++ "-to-" ++ show channelname1
    let (Just c) = searchChanList channelList channelname2
    sendOnChanList channelList channelname1 c
    execute channelList program
execute channelList (Split p q) = do
    id1 <- forkIO $ execute channelList p 
    id2 <- forkIO $ execute channelList q
    pure ()
execute channelList (Create channelname prog) = do
    chan <- mkChan $ "value:" ++ channelname
    execute ((channelname, chan) :channelList) prog
execute channelList nextprog@(Repeat prog) = do
    execute channelList prog
    yield
    threadDelay 100000  -- avoid fork bombs
    execute channelList nextprog
execute channelList Terminate = 
    pure ()


--search Pi'Value in ChannelList using ChannelName as index
searchChanList :: ChannelList -> ChannelName -> Maybe Pi'Value
searchChanList list chan = lookup chan list

--send Pi'Value (a channel with extra info) to a Pi'Value (Pi'Value is a channel and can be sent through Pi'Values)
sendOnChanList :: ChannelList -> ChannelName -> Pi'Value -> IO ()
sendOnChanList ((channelname',chanValue):xs) channelname msg | channelname == channelname' = writeChan (getChan chanValue) msg
sendOnChanList (x:xs) channelname msg = sendOnChanList xs channelname msg 
sendOnChanList [] channelname msg = do
    putStrLn $ "ERROR ON SEND : channel " ++ show channelname ++ " doesn't exist"
    undefined
    
--receive a Pi'Value from a Pi'Value
receiveOnChanList :: ChannelList -> ChannelName -> IO Pi'Value
receiveOnChanList ((channelname',chanValue):xs) channelname | channelname == channelname' = readChan (getChan chanValue)
receiveOnChanList (x:xs) channelname= receiveOnChanList xs channelname
receiveOnChanList [] channelname = do
    putStrLn $ "ERROR ON RECEIVE : channel " ++ show channelname ++ " doesn't exist"
    undefined



--parser

-- grammar 
-- terminate :    0 
--      example : 0
--
-- repeat :       !P
--      example : !0
--
-- newChannel :   (newx)P     on wikipedia : (vx)P
--      example : (new mychan)0
--
-- split :        {P}|{Q}     on wikipedia : P|Q
--      example : {0}|{0} 
--1more example : {!0}|{0}
--
-- send :         ^x<y>.P     on wikipedia : x<y>.P
--      example : ^general<bridge>.0   
--
-- receive :      x(y).P 
--      example : general(bridge).0

-- utility function to parse programs
parseProgram s = parse programP s

programP = 
  splitP  <|>
  sendP <|>
  receiveP <|>
  terminateP <|> 
  repeatP <|> 
  createP <|> 
  parenthesisP

-- this is a program without split: it is used to give split the highest priority
semiprogramP = 
  sendP <|>
  receiveP <|>
  terminateP <|> 
  repeatP <|> 
  createP <|> 
  parenthesisP

  
receiveP = do 
        -- channel to read over
        channel1 <- identifier
        char '('
        -- channel received
        channel2 <- identifier
        char ')'
        char '.'
        r <- semiprogramP 
        return $ Receive channel1 channel2 r

sendP = do
        char '^'
        -- channel to send over
        channel1 <- identifier
        char '<'
        -- channel to be sent
        channel2 <- identifier
        char '>'
        char '.'
        r <- semiprogramP
        return $ Send channel1 channel2 r

splitP = do 
        p <- semiprogramP
        char '|'
        q <- programP
        return $ Split p q

createP = do
        char '('
        string "new"
        --space
        channel <- identifier
        char ')'
        --space
        r <- semiprogramP 
        return $ Create channel r

terminateP = do
        char '0'
        return Terminate

repeatP = do
        char '!'
        r <- semiprogramP
        return $ Repeat r

parenthesisP = do
        char '('
        r <- programP
        char ')'
        return $ r


--message passing of input to result using channel passing


prova3 = let 
            a = "(new z)^x<z>.z(result).0"
            b = "(new input)x(chan).^chan<input>.0"
            c = "(new x)(" ++ a ++ "|" ++ b ++ ")"
         in c

prova2 =  "(new general)((new bridge)^general<bridge>.bridge(sink).!0|general(rcvBridge).^rcvBridge<general>.0)"
prova =  "(new x)((new y)^x<y>.y(z).!0|x(y).^y<x>.0)"


merge list = "(" ++ foldl (\x y -> x ++ "|" ++ y ) (head list) (tail list) ++ ")"
prova4 = exe $ ("(new fn)" ++ ) $ merge ["!fn(returnLabel).(new value)^returnLabel<value>.0", "(new retLab)^fn<retLab>.retLab(result).0"]

-- 
--replicante = "!(new token)^x<token>.0"
--nreplicanti x = take x $ repeat replicante
--fourmergedreplicanti = merge $ nreplicanti 4
--

--tests
tests =[
    ("simple'termination"       , "0"                           ),
    ("simple'repeat"            , "!0"                          ),
    ("simple'split"             , "0|0"                     ),
    ("simple'split'repeat"      , "!0|!0"                   ),
    ("simple'repeat'split"      , "!(0|0)"                    ),
    ("simple'create"            , "(new x)0"                    ),
    ("simple'send"              , "(new x)^x<x>.0"              ),
    ("simple'receive"           , "(new x)x(y).0"               ),
    ("simple'receive'send"      , "(new x)x(y).^y<y>.0"         ),
    ("simple'msg'passing"       , "(new x)(^x<x>.0|x(x).0)"   ),

    ("complex'1"       , "(new x)((new y)^x<y>.y(z).!0|x(y).^y<x>.0)"   ),
    ("complex'2"       , "(new general)((new bridge)^general<bridge>.bridge(sink).!0|general(rcvBridge).^rcvBridge<general>.0)"  ),


    ("ending test"              , "0"                           )]


-- run single test
runtest (name, program) = putStrLn name >> exe program
-- run all tests
runtests = foldr (\x y -> runtest x >> y) (putStrLn "end") tests

parsing'test = foldr (\(name, program) y -> putStr ">>" >> putStrLn name >> putStr "    " >> (putStrLn $ show $ parseProgram program) >> y) (putStrLn "end") tests














--